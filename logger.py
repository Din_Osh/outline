from time import time


class Logs(object):
    def __init__(self):
        super(Logs, self).__init__()
        self.logs = []
        self.logs.append((time(), "Init"))

    def add(self, txt):
        self.logs.append((time(), txt))

        # for python 3x
        print(txt)

        # for python 2x
        # print txt

    def getLogs(self):
        res = []
        for n in range(len(self.logs)):
            if n > 0:
                t, txt = self.logs[n]
                t1, _ = self.logs[n-1]
                secs = round(t - t1, 2)
                res.append("%s Secs: %s" % (secs, txt))
        total_time = self.logs[len(self.logs)-1][0] - self.logs[0][0]
        res.append("total: %s" % str(roung(total_time, 2)))
        return res

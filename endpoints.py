import logger
import os
import sys

import image_loader
from Inception_model import classifier
from Pre_process import aligner
from CGAN_model import cgan
from End_process import detector


LABELS = ['front', 'front_3_quarter', 'side', 'rear_3_quarter', 'rear', 'interior', 'tire']


def ml_progress(work_dir):

    # """ Step 1 : Classify with inception model ============================================================ """

    sys.stdout.write("Classify...\n")
    input_dir = work_dir
    output_dir = work_dir
    model_dir = "./Inception_model/model"
    sys.stdout.write("    In : {}\n".format(input_dir))
    sys.stdout.write("    Out : {}\n".format(output_dir))
    sys.stdout.write("    Model : {}\n".format(model_dir))

    n_list = classifier.classify(input_dir, output_dir, model_dir)

    """
    +----------+----------+----------+----------+----------+----------+----------+
    |    front |  front_3 |     side |   rear_3 |     rear | interior |     tire |
    +----------+----------+----------+----------+----------+----------+----------+
    |         0|         0|         0|         0|         0|         0|         0|
    +----------+----------+----------+----------+----------+----------+----------+
    """
    str_splite = ""
    str_classify_result = ""
    for n in n_list:
        str_classify_result += "|{:10}".format(n)
        str_splite += "+{:-<10}".format('-')
    sys.stdout.write(str_splite + "+\n")
    sys.stdout.write(
        "|{:10}|{:10}|{:10}|{:10}|{:10}|{:10}|{:10}|\n".format('front', 'front_3', 'side', 'rear_3', 'rear', 'interior',
                                                               'tire'))

    sys.stdout.write(str_splite + "+\n")
    sys.stdout.write(str_classify_result + "|\n")
    sys.stdout.write(str_splite + "+\n")

    # """ ======================================================================================================== """
    total_result_dics = []
    # for i in range(1, 3):  # only "side" and "front_3_quarter" model
    for i in range(len(n_list) - 2):

        if n_list[i] != 0:
            label = LABELS[i]
        else:
            continue

        sys.stdout.write("{}...\n".format(label))

        """ Step 2 : Combine for cgan(pix2pix) processing ======================================================== """
        sys.stdout.write("PreProcess. \n")
        in_dir = os.path.join(output_dir, label + "/")
        out_dir = os.path.join(in_dir, "combine/")
        sys.stdout.write("    In : {}\n".format(in_dir))
        sys.stdout.write("    Out : {}\n".format(out_dir))
        aligner.align_combine(in_dir, out_dir)

        """ Step 3 : cgan processing(main ml processing) ========================================================= """
        sys.stdout.write("Main ML process \n")
        in_dir = out_dir
        out_dir = in_dir
        model_dir = "./CGAN_model/model/" + label
        sys.stdout.write("    In : {}\n".format(in_dir))
        sys.stdout.write("    Out : {}\n".format(out_dir))
        sys.stdout.write("    Model : {}\n".format(model_dir))
        cgan.main(in_dir, out_dir, model_dir)

        """ Step 4 : cgan processing(main ml processing) ========================================================= """
        sys.stdout.write("End Process \n")
        origin_dir = os.path.join(output_dir, label + "/")
        croped_dir = os.path.join(out_dir, "images/")
        sys.stdout.write("    Origin : {}\n".format(in_dir))
        sys.stdout.write("    Cropped : {}\n".format(out_dir))
        # result image and upload to s3
        result_dicts = detector.recog(origin_dir, croped_dir, label)
        # result json dict
        total_result_dics.extend(result_dicts)

    # print("total_result_dics : ")
    # print(total_result_dics)
    return total_result_dics


VEHICLES = "vehicles"  # -> RESULTS = "results"
RESULTS = "results"
IMAGES = "images"

URL = "url"  # -> ["vehicle_points", "vehicle_color", "image_text", "image_classification", "image_url"]


def create_final_json(dealer_list, dict_list):

    for i in range(len(dealer_list)):

        dealer_list[i][RESULTS] = dealer_list[i].pop(VEHICLES)
        dealer = dealer_list[i]

        for j in range(len(dealer[RESULTS])):

            vehicle = dealer[RESULTS][j]
            for k in range(len(vehicle[IMAGES]) - 1, -1, -1):
                image = vehicle[IMAGES][k]
                base = os.path.basename(image[URL])
                basename, _ = os.path.splitext(base)

                bfind = False
                for dic in dict_list:
                    if dic["image_url"].find(basename) != -1:
                        (vehicle[IMAGES][k]).update(dic)
                        bfind = True
                        break

                # if not find in result dict list(processed images by ml processor)
                if not bfind:
                    vehicle[IMAGES].pop(k)

            dealer[RESULTS][j] = vehicle

        dealer_list[i] = dealer

    return dealer_list


def progress(work_dir, json_fname):
    # logs = logger.Logs()

    # Loading the image from its urls """

    sys.stdout.write("""\n
    ===============================================================
    Load images from urls
    ===============================================================\n""")
    dealers, urls, img_fns = image_loader.load(json_fname, work_dir)

    sys.stdout.write("Loaded images / Number of urls : {}/{}\n".format(len(img_fns), len(urls)))

    # ML processing with the loaded images"""
    sys.stdout.write("""\n
    ===============================================================
    START OUTLINE progress
    ===============================================================\n""")

    result_dicts = ml_progress(work_dir)

    # make a final json with dealers
    # print("dealers")
    # print(dealers)

    # print("result_dicts")
    # print(result_dicts)

    final_json_dict = create_final_json(dealers, result_dicts)
    # print("final_dics")
    # print(final_json_dict)
    sys.stdout.write("""\n
    ===============================================================
    END OUTLINE progress sucessfully!
    ===============================================================\n""")

    return final_json_dict

"""
 /| /|  /
/ |/ | /___  IMAGE PROCESSIMG
"""
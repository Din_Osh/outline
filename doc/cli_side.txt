# align
python Pre_process/align_process.py \
  --input_dir data/samples/side \
  --mode align \
  --size 256

# combine  
python CGAN_model/tools/process.py \
  --input_dir data/samples/side/A \
  --b_dir data/samples/side/B \
  --operation combine \
  --output_dir data/samples/side/combined

	(ignore)
		# split  
		python CGAN_model/tools/split.py \
		  --dir data/samples/side/combined

	""""""""""""""""""""""""""""""""""""""""	

	working on /combined
			not /train or /val
	"""""""""""""""""""""""""""""""""""""""""

# init training
python CGAN_model/pix2pix.py \
  --mode train \
  --output_dir CGAN_model/model/side \
  --max_epochs 500 \
  --input_dir data/samples/side/combined \
  --which_direction AtoB 

# from checkpoints
python CGAN_model/pix2pix.py \
  --mode train \
  --output_dir CGAN_model/model/side \
  --max_epochs 500 \
  --input_dir data/samples/side/combined \
  --which_direction AtoB \
  --checkpoint CGAN_model/model/side
  --lr 0.00002 (optional)


# validateion test
python CGAN_model/pix2pix.py \
  --mode test \
  --output_dir input_dir data/samples/side/combined/test_result\
  --input_dir input_dir data/samples/side/combined/val\
  --checkpoint CGAN_model/model/side

# test
python CGAN_model/pix2pix.py \
  --mode test \
  --output_dir input_dir data/samples/side/combined/test_result\
  --input_dir input_dir data/samples/side/combined/val\
  --checkpoint CGAN_model/model/side
  
# endprocesing
python End_process/detect_process.py \
  --origin_dir data/temp \
  --output_dir data/temp/output/images \
  --size 256

===MAIN PROCESSING========================================
# preprocessing
python Pre_process/align_process.py \
  --mode combine \
  --input_dir data/temp/ \
  --output_dir data/temp/output \
  --size 256

# main process/ML processing
python CGAN_model/pix2pix.py \
  --mode test \
  --output_dir data/temp/output\
  --input_dir data/temp/output\
  --checkpoint CGAN_model/model/side

# endprocesing
python End_process/detect_process.py \
  --origin_dir data/temp \
  --output_dir data/temp/output/images \
  --size 256


===CURRENT CHECKPOINT ====================================
progress  epoch 405  step 1852  image/sec 4.5  remaining 3653m
discrim_loss 3.59875
gen_loss_GAN 4.60534
gen_loss_L1 0.0144088






# align
python Pre_process/align_process.py \
  --input_dir data/samples/side \
  --mode align \
  --size 256

# combine  
python CGAN_model/tools/process.py \
  --input_dir data/samples/side/A \
  --b_dir data/samples/side/B \
  --operation combine \
  --output_dir data/samples/side/combined

# split  
python CGAN_model/tools/split.py \
  --dir data/samples/side/combined

# init training
python CGAN_model/pix2pix.py \
  --mode train \
  --output_dir CGAN_model/model/side \
  --max_epochs 500 \
  --input_dir data/samples/side/combined/train \
  --which_direction AtoB \

# from checkpoints
python CGAN_model/pix2pix.py \
  --mode train \
  --output_dir CGAN_model/model/side \
  --max_epochs 500 \
  --input_dir data/samples/side/combined/train \
  --which_direction AtoB \
  --checkpoint CGAN_model/model/side

python CGAN_model/pix2pix.py \
  --mode train \
  --output_dir CGAN_model/model/side \
  --max_epochs 500 \
  --input_dir data/samples/side/combined/train \
  --which_direction AtoB \
  
# test
python CGAN_model/pix2pix.py \
  --mode test \
  --output_dir input_dir data/samples/side/combined/test_result\
  --input_dir input_dir data/samples/side/combined/val\
  --checkpoint CGAN_model/model/side
  
# endprocesing
python End_process/detect_process.py \
  --origin_dir data/temp \
  --output_dir data/temp/output/images \
  --size 256